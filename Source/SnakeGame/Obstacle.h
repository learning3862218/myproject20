// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "DynamicActor.h"
#include "Obstacle.generated.h"

class ASnakeBase;

UCLASS()
class SNAKEGAME_API AObstacle : public ADynamicActor, public IInteractable
{
	GENERATED_BODY()

private:
	ASnakeBase* Snake;
public:	
	// Sets default values for this actor's properties
	AObstacle();
	UPROPERTY(EditDefaultsOnly);
	float LifeTime = 20.f;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;
};
