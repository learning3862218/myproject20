// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "SnakeElementBase.generated.h"

class UStaticMeshComponent;
class ASnakeBase;
enum class EMovementDirection;

UCLASS()
class SNAKEGAME_API ASnakeElementBase : public AActor, public IInteractable
{
	GENERATED_BODY()

private:
	
public:	
	// Sets default values for this actor's properties
	ASnakeElementBase();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
	UStaticMeshComponent* MeshComponent;

	UPROPERTY()
	ASnakeBase* SnakeOwner;
protected:
	FVector DeltaMovement = FVector(0.f, 0.f, 0.f);
	EMovementDirection MovementDirection;
	float Interval = 0.f;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintNativeEvent)
	void SetFirstElementType();
	void SetFirstElementType_Implementation();
	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	UFUNCTION()
	void HandleBeginOverlap(UPrimitiveComponent* OverlapedComponent, 
							AActor* OtherActor, 
							UPrimitiveComponent* OtherComp, 
							int32 OtherBodyIndex, 
							bool bFromSweep, 
							const FHitResult &SweepResult);

	UFUNCTION()
		void DeleteSnakeElement();

	UFUNCTION()
		void SetInterval(float NewInterval);

	UFUNCTION()
		void SetMovementDirection(EMovementDirection MovementDirection);
	UFUNCTION()
		EMovementDirection GetMovementDirection();
	UFUNCTION()
		void UpdateMovement();
	UFUNCTION()
		FVector GetDeltaMovement();

};
