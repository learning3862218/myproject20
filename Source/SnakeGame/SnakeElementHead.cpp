// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeElementHead.h"
#include "Kismet/GameplayStatics.h"
#include "SnakeGameInstance.h"
#include "PlayerPawnBase.h"
#include "SnakeBase.h"

void ASnakeElementHead::BeginPlay()
{
	Super::BeginPlay();
	World = GetWorld();
	GameInstance = Cast<USnakeGameInstance>(World->GetGameInstance());
	Pawn = Cast<APlayerPawnBase>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
}

void ASnakeElementHead::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (IsValid(World))
	{
		if (IsValid(GameInstance))
		{
			Interval = GameInstance->FieldElementSize * UGameplayStatics::GetWorldDeltaSeconds(World) * GameInstance->Speed;
		}

	}
	if (Pawn->SnakeActor->IsDecrease)
	{
		Pawn->SnakeActor->DeleteLastElement();
		Pawn->SnakeActor->IsDecrease = false;
	}

	for (int i = 0; i < Pawn->SnakeActor->SnakeElements.Num(); ++i)
	{
		Pawn->SnakeActor->SnakeElements[i]->SetInterval(Interval);
		Pawn->SnakeActor->SnakeElements[i]->UpdateMovement();
		Pawn->SnakeActor->SnakeElements[i]->AddActorWorldOffset(Pawn->SnakeActor->SnakeElements[i]->GetDeltaMovement());
	}

	//�������� ������ ������ �� ����
	if (IsValid(SnakeOwner))
	{

		if (IsValid(Pawn))
		{
			FVector CameraLocation = Pawn->GetActorLocation();
			FVector SnakeLocation = this->GetActorLocation();
			CameraLocation.X = SnakeLocation.X;
			CameraLocation.Y = SnakeLocation.Y;
			Pawn->SetActorLocation(CameraLocation);
		}
	}

}

ASnakeElementHead::ASnakeElementHead()
{
	SetMovementDirection(EMovementDirection::UP);
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementHead::HandleBeginOverlap);
}

void ASnakeElementHead::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		auto SnakeElement = Cast<ASnakeElementBase>(Interactor);
		if (Snake->SnakeElements[1] != this)
		{
			Snake->KillSnake();
		}
	}
}
