#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.h"
#include "SpawnerBase.generated.h"

class AFood;
class ADecrease;
class AZoomIn;
class AZoomOut;
class AObstacle;
class UWorld;
class USnakeGameInstance;


UCLASS()
class SNAKEGAME_API ASpawnerBase : public AActor
{
	GENERATED_BODY()

private:
	
	UPROPERTY(EditDefaultsOnly)
	int MaxObjectsCount = 20;
	int CurrentObjectsCount;
	int Width;
	int Height;
	float Step;
	float XOffset;
	float YOffset;
	UPROPERTY()
	UWorld* World;
	UPROPERTY()
	USnakeGameInstance* GameInstance;
	UPROPERTY()
	ASnakeBase* SnakePtr;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> FoodClass;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ADecrease> DecreaseClass;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AZoomIn> ZoomInClass;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AZoomOut> ZoomOutClass;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AObstacle> ObstacleClass;

	UFUNCTION()
	bool TraceObstacles(FVector& TraceLocation);

public:
	// Sets default values for this actor's properties
	ASpawnerBase();
	~ASpawnerBase();

	UFUNCTION()
	void Update();

	UFUNCTION()
	void DecreaseCurrentObjectsCount();

	UFUNCTION()
	void IncreaseCurrentObjectsCount();

	UFUNCTION()
	void SetSnakePtr(ASnakeBase* Ptr);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
