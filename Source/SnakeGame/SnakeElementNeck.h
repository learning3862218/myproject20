// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "SnakeElementNeck.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME_API ASnakeElementNeck : public ASnakeElementBase
{
	GENERATED_BODY()
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	ASnakeElementNeck();
	virtual void Interact(AActor* Interactor, bool bIsHead) override;
};
