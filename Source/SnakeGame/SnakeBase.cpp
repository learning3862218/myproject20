// Fill out your copyright notice in the Description page of Project Settings.

#include "SnakeBase.h"
#include "Kismet/GameplayStatics.h"
#include "PlayerPawnBase.h"
#include "SnakeElementBase.h"
#include "SnakeElementHead.h"
#include "SnakeElementNeck.h"
#include "SnakeElementBody.h"
#include "SnakeGameInstance.h"
#include "Interactable.h"
#include "Obstacle.h"
#include "SpawnerBase.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();

	World = GetWorld();
	if (!World) return;

	GameInstance = Cast<USnakeGameInstance>(World->GetGameInstance());
	if (!GameInstance) return;

	SetActorLocation(FVector(0.f, 0.f, ElementSize / 2.f));
	SetActorTickInterval(1.0f / GameInstance->Speed);
	AddSnakeElement(6);
	Spawner = GetWorld()->SpawnActor<ASpawnerBase>(SpawnerClass);
	Spawner->SetSnakePtr(this);
	//BoundingAreaSpawner();
	LastMovementDirection = EMovementDirection::UP;
}

void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (IsFood)
	{
		AddSnakeElement(1);
		IsFood = false;
	}
	Move();
	Spawner->Update();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	// ���������� ������ �������� � ��� ������������ ����
	if (!World) return;

	if (ElementsNum == 1 && SnakeElements.Num() > 0)
	{
		if (GameInstance)
		{
			EMovementDirection LastElementDirection = SnakeElements.Last()->GetMovementDirection();
			float Offset = GameInstance->FieldElementSize;
			FVector OffsetVector = FVector::ZeroVector;
			switch (LastElementDirection)
			{
			case EMovementDirection::UP:
				OffsetVector.X -= Offset;
				break;
			case EMovementDirection::DOWN:
				OffsetVector.X += Offset;
				break;
			case EMovementDirection::LEFT:
				OffsetVector.Y -= Offset;
				break;
			case EMovementDirection::RIGHT:
				OffsetVector.Y += Offset;
				break;
			}
			FTransform NewTransform(SnakeElements.Last()->GetActorLocation());
			NewTransform.SetLocation(NewTransform.GetLocation() + OffsetVector);
			ASnakeElementBase* NewSnakeElement;
			if (SnakeElements.Num() < NeckElementsCount + 1)
			{
				NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementNeck>(SnakeElementNeckClass, NewTransform);
			}
			else
			{
				NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBody>(SnakeElementBodyClass, NewTransform);
			}
			NewSnakeElement->SnakeOwner = this;
			NewSnakeElement->SetMovementDirection(LastElementDirection);
			SnakeElements.Add(NewSnakeElement);
		}
	return;
	}
	//�������� ����
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(GetActorLocation() - NewLocation);
		ASnakeElementBase* NewSnakeElement;
		if (i == 0)
		{
			NewSnakeElement = World->SpawnActor<ASnakeElementHead>(SnakeElementHeadClass, NewTransform);
		}
		else if (i <= NeckElementsCount)
		{
			NewSnakeElement = World->SpawnActor<ASnakeElementNeck>(SnakeElementNeckClass, NewTransform);
		}
		else
		{
			NewSnakeElement = World->SpawnActor<ASnakeElementBody>(SnakeElementBodyClass, NewTransform);
		}
		NewSnakeElement->SnakeOwner = this;
		SnakeElements.Add(NewSnakeElement);
	}
}

void ASnakeBase::Move()
{
	SetIsMoving(true);
	FVector MovementVector(FVector::ZeroVector);
	float Interval = 100.0; // MovementSpeed* ElementSize* ASnakeBase::GetActorTickInterval();
	switch (LastMovementDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += Interval;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= Interval;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += Interval;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= Interval;
		break;
	}

	if (SnakeElements.Num() > 1)
	{
		for (int i = SnakeElements.Num() - 1; i > 0; --i)
			SnakeElements[i]->SetMovementDirection(SnakeElements[i - 1]->GetMovementDirection());
	}
	SnakeElements[0]->SetMovementDirection(LastMovementDirection);
	FixLocation();
	SetIsMoving(false);
}

bool ASnakeBase::IsMoving()
{
	return Moving;
}

void ASnakeBase::SetIsMoving(bool moving)
{
	Moving = moving;
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::KillSnake()
{
	for (auto Element : SnakeElements)
	{
		Element->DeleteSnakeElement();
	}
	this->Destroy();

}

void ASnakeBase::DeleteLastElement()
{
	if (SnakeElements.Num() > 1)
	{
		SnakeElements[SnakeElements.Num() - 1]->DeleteSnakeElement();
		SnakeElements.RemoveAtSwap(SnakeElements.Num() - 1);
	}
}

void ASnakeBase::FixLocation()
{
	float Step = 100.f;
	if (IsValid(World))
	{
		if (IsValid(GameInstance))
		{
			Step = GameInstance->FieldElementSize;
		}
	}

	for (int i = 0; i < SnakeElements.Num(); ++i)
	{
		FVector Location = SnakeElements[i]->GetActorLocation();
		Location.X = floor((Location.X + Step * 0.1) / Step) * Step;
		Location.Y = floor((Location.Y + Step * 0.1) / Step) * Step;
		SnakeElements[i]->SetActorLocation(Location);
	}
}

void ASnakeBase::BoundingAreaSpawner()
{
	if (World)
	{
		if (GameInstance)
		{
			int Width = GameInstance->FieldWidth;
			int Height = GameInstance->FieldHeight;
			float Step = GameInstance->FieldElementSize;
			float XOffset = (Width * Step) / 2.0;
			float YOffset = (Height * Step) / 2.0;
			for (int i = 0; i < Width; ++i)
			{
				FVector Location = FVector(YOffset, i * Step - XOffset, 100);
				FTransform Transform;
				Transform.SetLocation(Location);
				AObstacle* Obstacle = World->SpawnActor<AObstacle>(ObstacleClass, Transform);
			}

		}

	}

}

