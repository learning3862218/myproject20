// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeElementBody.h"
#include "SnakeBase.h"

void ASnakeElementBody::BeginPlay()
{
	Super::BeginPlay();
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBody::HandleBeginOverlap);
}

ASnakeElementBody::ASnakeElementBody()
{
	PrimaryActorTick.bCanEverTick = false;
}

void ASnakeElementBody::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		auto SnakeElement = Cast<ASnakeElementBase>(Interactor);
		if (Snake->SnakeElements[1] != this)
		{
			Snake->KillSnake();
		}
	}
}