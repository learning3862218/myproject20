// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "SnakeElementBody.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME_API ASnakeElementBody : public ASnakeElementBase
{
	GENERATED_BODY()
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	ASnakeElementBody();
	virtual void Interact(AActor* Interactor, bool bIsHead) override;
	
};
