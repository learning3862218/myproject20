// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;
class AObstacle;
class ASpawnerBase;
class ASpawnerBase;
class ASnakeElementHead;
class ASnakeElementNeck;
class ASnakeElementBody;
class UWorld;
class USnakeGameInstance;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()
private:
	bool Moving = false;
	UWorld* World;
	USnakeGameInstance* GameInstance;
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AObstacle> ObstacleClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASpawnerBase> SpawnerClass;
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementHead> SnakeElementHeadClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementNeck> SnakeElementNeckClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBody> SnakeElementBodyClass;

	UPROPERTY(EditDefaultsOnly)
	int NeckElementsCount = 4;

	float ElementSize;

	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;
	
	UPROPERTY()
	ASpawnerBase* Spawner;

	UPROPERTY()
	EMovementDirection LastMovementDirection;

	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed;

	bool IsFood = false;
	bool IsDecrease = false;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	UFUNCTION(BlueprintCallable)
	void AddSnakeElement(int ElementsNum = 1);
	
	UFUNCTION(BlueprintCallable)
	void Move();
	
	bool IsMoving();

	void SetIsMoving(bool moving);

	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);

	void KillSnake();

	void DeleteLastElement();

	void FixLocation();

	UFUNCTION()
	void BoundingAreaSpawner();
};
