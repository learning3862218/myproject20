// Fill out your copyright notice in the Description page of Project Settings.

#include "ZoomIn.h"
#include "Kismet/GameplayStatics.h"
#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "PlayerPawnBase.h"
#include "SpawnerBase.h"
#include "SnakeBase.h"

// Sets default values
AZoomIn::AZoomIn()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AZoomIn::BeginPlay()
{
	Super::BeginPlay();
	Zoomed = false;
}

// Called every frame
void AZoomIn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (Zoomed && IsValid(PlayerPawn))
	{
		FVector CameraLocation = PlayerPawn->PawnCamera->GetComponentLocation();
		CameraLocation.Z -= PlayerPawn->STEP_HEIGHT * ZoomStep;
		PlayerPawn->PawnCamera->SetWorldLocation(CameraLocation);
		if (CameraLocation.Z <= NewCameraLocationZ)
		{
			Snake->Spawner->DecreaseCurrentObjectsCount();
			this->Destroy();
		}
	}

}

void AZoomIn::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->Spawner->DecreaseCurrentObjectsCount();
			PlayerPawn = Cast<APlayerPawnBase>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
			if (IsValid(PlayerPawn))
			{
				FVector CameraLocation = PlayerPawn->PawnCamera->GetComponentLocation();
				NewCameraLocationZ = CameraLocation.Z - PlayerPawn->STEP_HEIGHT;
				if (NewCameraLocationZ < PlayerPawn->MIN_HEIGHT)
				{
					NewCameraLocationZ = PlayerPawn->MIN_HEIGHT;
				}
				this->MeshComponent->DestroyComponent();
				Zoomed = true;
			}
		}
	}
}

