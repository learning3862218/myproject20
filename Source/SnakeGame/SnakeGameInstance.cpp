// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeGameInstance.h"
#include "Engine.h"

void USnakeGameInstance::Init()
{
	Super::Init();
	GEngine->GameUserSettings->SetVSyncEnabled(true);
	GEngine->GameUserSettings->ApplySettings(true);
	GEngine->GameUserSettings->SaveSettings();
	GEngine->Exec(GetWorld(), TEXT("t,MaxFPS 60"));
}

void USnakeGameInstance::AddPoints(int PointsAdded)
{
	CurrentPoints += PointsAdded;
}
